/* waker-config.h
 *
 * Copyright 2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "waker-device.h"

G_BEGIN_DECLS

#define WAKER_TYPE_CONFIG (waker_config_get_type())

G_DECLARE_FINAL_TYPE(WakerConfig, waker_config, WAKER, CONFIG, GObject)

gboolean waker_config_load(WakerConfig *config);

gboolean waker_config_save(WakerConfig *config);

gboolean waker_config_add(WakerConfig *config, WakerDevice *device);

gboolean waker_config_edit(WakerConfig *config, const gchar *name,
			   WakerDevice *device);

gboolean waker_config_remove(WakerConfig *config, const gchar *name);

/**
 * Find device by name of @p name
 *
 * @param name
 * @return WakerDevice
 */
WakerDevice *waker_config_find_device(WakerConfig *config, const gchar *name);

/**
 * Get all configured devices
 *
 * @return GList *
 */
GList *waker_config_devices(WakerConfig *config);

G_END_DECLS