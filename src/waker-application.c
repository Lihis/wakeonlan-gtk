/* waker-application.c
 *
 * Copyright 2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "waker-application.h"
#include "waker-window.h"

struct _WakerApplication {
	AdwApplication parent_instace;

	WakerConfig *config;
};

G_DEFINE_TYPE(WakerApplication, waker_application, ADW_TYPE_APPLICATION)

WakerApplication *waker_application_new(WakerConfig *config)
{
	WakerApplication *self = NULL;

	g_assert_nonnull(config);

	self = g_object_new(WAKER_TYPE_APPLICATION, "application-id",
			    "net.lihis.lan-waker", "flags",
			    G_APPLICATION_DEFAULT_FLAGS, NULL);
	self->config = config;

	return self;
}

static void waker_application_class_finalize(GObject *gobject)
{
	G_OBJECT_CLASS(waker_application_parent_class)->finalize(gobject);
}

static void waker_application_on_activate(GApplication *app)
{
	WakerApplication *self;
	GtkWindow *window;

	g_assert(GTK_IS_APPLICATION(app));
	g_assert(WAKER_IS_APPLICATION(app));

	self = WAKER_APPLICATION(app);
	if (waker_config_load(self->config) == FALSE)
		g_warning("Failed to load configuration");

	window = gtk_application_get_active_window(GTK_APPLICATION(app));
	if (window == NULL) {
		window = GTK_WINDOW(
			waker_window_new(ADW_APPLICATION(self), self->config));
	}

	gtk_window_present(window);
}

static void waker_application_class_init(WakerApplicationClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GApplicationClass *app_class = G_APPLICATION_CLASS(klass);

	gobject_class->finalize = waker_application_class_finalize;
	app_class->activate = waker_application_on_activate;
}

static void waker_application_show_about(GSimpleAction *action G_GNUC_UNUSED,
					 GVariant *parameter G_GNUC_UNUSED,
					 gpointer user_data)
{
	WakerApplication *self = WAKER_APPLICATION(user_data);
	GtkWindow *window;
	const gchar *authors[] = { "Tomi Lähteenmäki", NULL };

	g_return_if_fail(WAKER_IS_APPLICATION(self));

	window = gtk_application_get_active_window(GTK_APPLICATION(self));

	gtk_show_about_dialog(window, "program-name", "LAN Waker", "authors",
			      authors, "version", "0.1.0", NULL);
}

static void waker_application_init(WakerApplication *self)
{
	g_assert(WAKER_IS_APPLICATION(self));

	self->config = NULL;

	g_autoptr(GSimpleAction) quit_action =
		g_simple_action_new("quit", NULL);
	g_signal_connect_swapped(quit_action, "activate",
				 G_CALLBACK(g_application_quit), self);
	g_action_map_add_action(G_ACTION_MAP(self), G_ACTION(quit_action));

	g_autoptr(GSimpleAction) about_action =
		g_simple_action_new("about", NULL);
	g_signal_connect(about_action, "activate",
			 G_CALLBACK(waker_application_show_about), self);
	g_action_map_add_action(G_ACTION_MAP(self), G_ACTION(about_action));

	gtk_application_set_accels_for_action(GTK_APPLICATION(self), "app.quit",
					      (const char *[]){ "<primary>q",
								NULL });
}