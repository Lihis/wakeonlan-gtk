/* waker-device.h
 *
 * Copyright 2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define WAKER_TYPE_DEVICE (waker_device_get_type())

G_DECLARE_FINAL_TYPE(WakerDevice, waker_device, WAKER, DEVICE, GObject)

const gchar *waker_device_name(WakerDevice *device);
void waker_device_set_name(WakerDevice *device, const gchar *name);

const gchar *waker_device_mac(WakerDevice *device);
void waker_device_set_mac(WakerDevice *device, const gchar *mac);

const gchar *waker_device_host(WakerDevice *device);
void waker_device_set_host(WakerDevice *device, const gchar *host);

guint16 waker_device_port(WakerDevice *device);
void waker_device_set_port(WakerDevice *device, guint16 port);

const gchar *waker_device_secure_on(WakerDevice *device);
void waker_device_set_secure_on(WakerDevice *device, const gchar *secure_on);

void waker_device_update(WakerDevice *device, WakerDevice *from);

G_END_DECLS