/* waker-window.c
 *
 * Copyright 2020-2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "waker-window.h"
#include "waker-wol.h"
#include "waker-device-config.h"
#include <glib/gi18n.h>
#include <adwaita.h>

struct _WakerWindow {
	AdwApplicationWindow parent_instance;

	/* Template widgets */
	AdwHeaderBar *header_bar;
	GtkToggleButton *toggle_pane_button;
	GtkButton *delete;
	AdwFlap *flap;
	WakerDeviceConfig *device_config;
	GtkListBox *listbox;

	/* Other */
	WakerConfig *config;
	GtkListBoxRow *listbox_selection;
};

G_DEFINE_TYPE(WakerWindow, waker_window, ADW_TYPE_APPLICATION_WINDOW)

static void waker_window_on_wake_clicked(GtkButton *button G_GNUC_UNUSED,
					 gpointer user_data)
{
	waker_wol_wake(WAKER_DEVICE(user_data));
}

static GtkWidget *waker_window_create_row(WakerDevice *device)
{
	AdwActionRow *row = NULL;
	GtkWidget *button = NULL;

	row = ADW_ACTION_ROW(adw_action_row_new());
	g_object_set(row, "title", waker_device_name(device), NULL);
	adw_action_row_set_subtitle(row, waker_device_mac(device));
	button = gtk_button_new_with_label(_("Wake"));
	g_signal_connect(button, "clicked",
			 G_CALLBACK(waker_window_on_wake_clicked), device);
	adw_action_row_add_suffix(row, button);

	return GTK_WIDGET(row);
}

static void waker_window_update_devices(WakerWindow *self)
{
	GtkListBoxRow *row = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	gtk_widget_set_sensitive(GTK_WIDGET(self->toggle_pane_button), FALSE);

	row = gtk_list_box_get_row_at_index(self->listbox, 0);
	while (row != NULL) {
		gtk_list_box_remove(self->listbox, GTK_WIDGET(row));
		row = gtk_list_box_get_row_at_index(self->listbox, 0);
	}

	for (GList *iter = waker_config_devices(self->config); iter != NULL;
	     iter = iter->next) {
		WakerDevice *device = iter->data;
		GtkWidget *row;

		row = waker_window_create_row(device);
		gtk_list_box_insert(self->listbox, row, -1);
	}
}

WakerWindow *waker_window_new(AdwApplication *app, WakerConfig *config)
{
	WakerWindow *self;

	self = g_object_new(WAKER_TYPE_WINDOW, "application", app, NULL);
	self->config = config;
	waker_window_update_devices(self);

	return self;
}

static void waker_window_on_dialog_response(GtkDialog *dialog, gint response_id,
					    gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	GtkWidget *content_area = NULL;
	GtkWidget *device_config = NULL;
	WakerDevice *device = NULL;
	g_autofree gchar *error = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	if (response_id != GTK_RESPONSE_ACCEPT) {
		gtk_window_destroy(GTK_WINDOW(dialog));
		return;
	}

	content_area = gtk_dialog_get_content_area(dialog);
	device_config = gtk_widget_get_first_child(content_area);
	device = waker_device_config_get(WAKER_DEVICE_CONFIG(device_config),
					 &error);
	waker_config_add(self->config, device);
	if (waker_config_save(self->config) != TRUE) {
		g_error("Failed to save configuration");
	}

	waker_window_update_devices(self);

	gtk_window_destroy(GTK_WINDOW(dialog));
}

static void waker_window_on_new_clicked(GSimpleAction *simple G_GNUC_UNUSED,
					GVariant *parameter G_GNUC_UNUSED,
					gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	WakerDeviceConfig *device_config = NULL;
	GtkWidget *dialog = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	device_config = waker_device_config_new();
	dialog = gtk_dialog_new_with_buttons(_("New device"), GTK_WINDOW(self),
					     GTK_DIALOG_DESTROY_WITH_PARENT,
					     _("_Add"), GTK_RESPONSE_ACCEPT,
					     _("_Cancel"), GTK_RESPONSE_REJECT,
					     NULL);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);

	g_signal_connect(dialog, "response",
			 G_CALLBACK(waker_window_on_dialog_response), self);

	gtk_box_append(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))),
		       GTK_WIDGET(device_config));
	gtk_widget_show(dialog);

	if (waker_config_save(self->config) != TRUE)
		g_printerr("Failed to save configuration\n");
}

static WakerDevice *waker_window_get_selected_device(WakerWindow *self)
{
	GList *selection = NULL;
	g_autofree gchar *name = NULL;
	WakerDevice *device = NULL;

	selection = gtk_list_box_get_selected_rows(self->listbox);
	if (selection) {
		g_object_get(selection->data, "title", &name, NULL);
		device = waker_config_find_device(self->config, name);
		if (device == NULL) {
			g_printerr("No such device \"%s\"\n", name);
		}
	}

	return device;
}

static void on_confirm_reponse(GtkDialog *dialog, gint response_id,
			       gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	WakerDevice *device = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	device = waker_window_get_selected_device(self);
	if (response_id != GTK_RESPONSE_ACCEPT || device == NULL) {
		gtk_window_close(GTK_WINDOW(dialog));
		return;
	}

	waker_config_remove(self->config, waker_device_name(device));
	waker_window_update_devices(self);
	adw_flap_set_reveal_flap(self->flap, FALSE);
	gtk_window_close(GTK_WINDOW(dialog));
}

static void waker_window_on_delete_clicked(GtkButton *button G_GNUC_UNUSED,
					   gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	WakerDevice *device = NULL;
	g_autofree gchar *device_name = NULL;
	GtkDialogFlags flags;
	GtkDialog *dialog = NULL;
	GtkWidget *button_remove = NULL;
	GtkStyleContext *button_context = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	device = waker_window_get_selected_device(self);
	if (device == NULL)
		return;

	device_name = g_strdup_printf("\"%s\"", waker_device_name(device));

	flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
	dialog = GTK_DIALOG(gtk_message_dialog_new(
		GTK_WINDOW(&self->parent_instance), flags, GTK_MESSAGE_QUESTION,
		GTK_BUTTONS_NONE, "Remove device?"));
	gtk_message_dialog_format_secondary_text(
		GTK_MESSAGE_DIALOG(dialog),
		_("Are you sure you want to remove %s?\n"), device_name);
	(void)gtk_dialog_add_button(dialog, _("Cancel"), GTK_RESPONSE_REJECT);
	button_remove =
		gtk_dialog_add_button(dialog, _("Remove"), GTK_RESPONSE_ACCEPT);
	button_context =
		gtk_widget_get_style_context(GTK_WIDGET(button_remove));
	gtk_style_context_add_class(button_context, "destructive-action");
	gtk_dialog_set_default_response(dialog, GTK_RESPONSE_REJECT);

	g_signal_connect(dialog, "response", G_CALLBACK(on_confirm_reponse),
			 self);

	gtk_widget_show(GTK_WIDGET(dialog));
}

static void waker_window_on_row_selected(GtkListBox *box G_GNUC_UNUSED,
					 GtkListBoxRow *row, gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	gboolean sensitive = FALSE;

	g_assert(WAKER_IS_WINDOW(self));

	sensitive = row ? TRUE : FALSE;
	self->listbox_selection = row;

	gtk_widget_set_sensitive(GTK_WIDGET(self->toggle_pane_button),
				 sensitive);

	if (sensitive == TRUE) {
		WakerDevice *device;

		device = waker_window_get_selected_device(self);
		waker_device_config_set(self->device_config, device);
	}
}

static void waker_window_dispose(GObject *gobject)
{
	WakerWindow *self = WAKER_WINDOW(gobject);

	g_signal_handlers_disconnect_by_func(
		G_OBJECT(self->listbox), waker_window_on_row_selected, self);

	G_OBJECT_CLASS(waker_window_parent_class)->dispose(gobject);
}

static void waker_window_class_init(WakerWindowClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	g_type_ensure(WAKER_TYPE_DEVICE_CONFIG);

	gtk_widget_class_set_template_from_resource(
		widget_class, "/net/lihis/lan-waker/waker-window.ui");

	gtk_widget_class_bind_template_child(widget_class, WakerWindow,
					     header_bar);
	gtk_widget_class_bind_template_child(widget_class, WakerWindow,
					     toggle_pane_button);
	gtk_widget_class_bind_template_child(widget_class, WakerWindow, delete);
	gtk_widget_class_bind_template_child(widget_class, WakerWindow, flap);
	gtk_widget_class_bind_template_child(widget_class, WakerWindow,
					     device_config);
	gtk_widget_class_bind_template_child(widget_class, WakerWindow,
					     listbox);

	gobject_class->dispose = waker_window_dispose;
}

static void show_error_dialog(WakerWindow *self, const gchar *error)
{
	GtkDialogFlags flags = 0;
	GtkWidget *dialog = NULL;

	g_assert(WAKER_IS_WINDOW(self));

	flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
	dialog = gtk_message_dialog_new(GTK_WINDOW(self), flags,
					GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
					_("Error"));
	gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
						 "%s", error);
	g_signal_connect(G_OBJECT(dialog), "response",
			 G_CALLBACK(gtk_window_close), NULL);
	gtk_widget_show(dialog);
}

static void waker_window_on_flap_reveal(GtkWidget *flap,
					gboolean value G_GNUC_UNUSED,
					gpointer user_data)
{
	WakerWindow *self = WAKER_WINDOW(user_data);
	gboolean revealed = FALSE;

	g_assert(WAKER_IS_WINDOW(self));

	revealed = adw_flap_get_reveal_flap(ADW_FLAP(flap));
	if (revealed == FALSE) {
		WakerDevice *device = NULL;
		g_autofree gchar *error = NULL;

		device = waker_device_config_get(self->device_config, &error);
		if (!device) {
			adw_flap_set_reveal_flap(self->flap, TRUE);
			show_error_dialog(self, error);
		} else {
			waker_config_edit(self->config,
					  waker_device_name(device), device);
			waker_config_save(self->config);
		}
	}
}

const GActionEntry win_actions[] = {
	{ "new", waker_window_on_new_clicked, NULL, NULL, NULL, {} },
};

static void waker_window_init(WakerWindow *self)
{
	gtk_widget_init_template(GTK_WIDGET(self));

	g_action_map_add_action_entries(G_ACTION_MAP(&self->parent_instance),
					win_actions, G_N_ELEMENTS(win_actions),
					self);

	g_signal_connect(self->flap, "notify::reveal-flap",
			 G_CALLBACK(waker_window_on_flap_reveal), self);

	g_signal_connect(self->delete, "clicked",
			 G_CALLBACK(waker_window_on_delete_clicked), self);

	g_signal_connect(self->listbox, "row-selected",
			 G_CALLBACK(waker_window_on_row_selected), self);
}
