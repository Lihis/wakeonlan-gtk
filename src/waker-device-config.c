/* waker-device-config.c
 *
 * Copyright 2020-2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "waker-device-config.h"
#include <adwaita.h>
#include <glib/gi18n.h>

struct _WakerDeviceConfig {
	GtkBox parent_istance;

	/* Template widgets */
	AdwEntryRow *name;
	AdwEntryRow *mac;
	AdwEntryRow *host;
	AdwEntryRow *port;
	//AdwEntryRow *secure_on;
};

G_DEFINE_TYPE(WakerDeviceConfig, waker_device_config, GTK_TYPE_BOX)

WakerDeviceConfig *waker_device_config_new()
{
	return g_object_new(WAKER_TYPE_DEVICE_CONFIG, NULL);
}

static void set_entry(AdwEntryRow *entry, const gchar *value)
{
	gtk_editable_set_text(GTK_EDITABLE(entry), value);
}

static void update_device(WakerDeviceConfig *self, WakerDevice *device)
{
	g_autofree gchar *port = NULL;

	port = g_strdup_printf("%d", waker_device_port(device));

	set_entry(self->name, waker_device_name(device));
	set_entry(self->mac, waker_device_mac(device));
	set_entry(self->host, waker_device_host(device));
	set_entry(self->port, port);
	//set_entry(self->secure_on, waker_device_secure_on(device));
}

WakerDeviceConfig *waker_device_config_new_from(WakerDevice *device)
{
	WakerDeviceConfig *self = waker_device_config_new();

	update_device(self, device);

	return self;
}

void waker_device_config_set(WakerDeviceConfig *self, WakerDevice *device)
{
	g_assert(WAKER_IS_DEVICE_CONFIG(self));
	g_assert_nonnull(device);

	update_device(self, device);
}

static const gchar *get_entry_text(AdwEntryRow *entry)
{
	return gtk_editable_get_text(GTK_EDITABLE(entry));
}

WakerDevice *waker_device_config_get(WakerDeviceConfig *self, gchar **error)
{
	WakerDevice *device = NULL;
	gint64 port = G_MAXINT64;

	g_assert(WAKER_IS_DEVICE_CONFIG(self));

	device = g_object_new(WAKER_TYPE_DEVICE, NULL);
	waker_device_set_name(device, get_entry_text(self->name));
	waker_device_set_mac(device, get_entry_text(self->mac));
	waker_device_set_host(device, get_entry_text(self->host));
	port = g_ascii_strtoll(get_entry_text(self->port), NULL, 10);
	if (port < 0 || port > G_MAXUINT16) {
		g_clear_object(&device);
		*error = g_strdup_printf(_("Port has to be in range of 0-%d"),
					 G_MAXUINT16);
	} else {
		waker_device_set_port(device, port);
	}
	//waker_device_set_secure_on(device, get_entry_text(self->secure_on));

	return device;
}

static void waker_device_config_class_init(WakerDeviceConfigClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	gtk_widget_class_set_template_from_resource(
		widget_class, "/net/lihis/lan-waker/waker-device-config.ui");

	gtk_widget_class_bind_template_child(widget_class, WakerDeviceConfig,
					     name);
	gtk_widget_class_bind_template_child(widget_class, WakerDeviceConfig,
					     mac);
	gtk_widget_class_bind_template_child(widget_class, WakerDeviceConfig,
					     host);
	gtk_widget_class_bind_template_child(widget_class, WakerDeviceConfig,
					     port);
}

static void waker_device_config_init(WakerDeviceConfig *self)
{
	gtk_widget_init_template(GTK_WIDGET(self));
}