/* waker-device-config.h
 *
 * Copyright 2020-2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "waker-device.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define WAKER_TYPE_DEVICE_CONFIG (waker_device_config_get_type())

G_DECLARE_FINAL_TYPE(WakerDeviceConfig, waker_device_config, WAKER,
		     DEVICE_CONFIG, GtkBox)

WakerDeviceConfig *waker_device_config_new(void);
WakerDeviceConfig *waker_device_config_new_from(WakerDevice *device);

void waker_device_config_set(WakerDeviceConfig *widget, WakerDevice *device);
WakerDevice *waker_device_config_get(WakerDeviceConfig *widget, gchar **error);

G_END_DECLS
