# LAN Waker

Simple Wake-on-LAN application for GNOME.

## Command Line Usage

Configured devices can be woken up without bringing up the GUI with
`--wake [DEVICE]` flag where `[DEVICE]` is the `Name` of the device in
configuration.

